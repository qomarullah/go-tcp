package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"time"

	rotatelogs "github.com/lestrrat/go-file-rotatelogs"
)

func mainx() {
	logPath := os.Getenv("LOG")
	if logPath == "" {
		logPath = "logs"
	}
	createLogPath(logPath)

	port := os.Getenv("PORT")
	if port == "" {
		port = "1883"
	}
	rl, _ := rotatelogs.New(logPath + "/log.%Y%m%d%H")
	log.SetOutput(rl)
	log.SetFlags(0)

	// listen on port 8000
	ln, _ := net.Listen("tcp", ":"+port)

	// accept connection
	conn, _ := ln.Accept()
	// run loop forever (or until ctrl-c)
	//go func() {
	defer func() {
		fmt.Println("Close the connection in ", conn.RemoteAddr())
		conn.Close()
	}()

	for {
		// get message, output
		message, _ := bufio.NewReader(conn).ReadString('\n')
		fmt.Print("Message Received:", string(message))
		event := &Event{Timestamp: time.Now().Format(time.RFC3339), Message: string(message)}
		eventLog, _ := json.Marshal(event)
		log.Println(string(eventLog))
	}
	//	}()

	/*
		fmt.Println("Start server...")
		http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
			//fmt.Fprintf(w, "Hello, %q", html.EscapeString(r.URL.Path))
			log.Println("ping", html.EscapeString(r.URL.Path))
		})

		portHttpInc, _ := strconv.Atoi(port)
		portHttp := strconv.Itoa(portHttpInc + 1)
		log.Fatal(http.ListenAndServe(":"+portHttp, nil))
	*/

}

type Event struct {
	Timestamp string `json:"timestamp"`
	//Topic     string `json:"topic"`
	Message string `json:"message"`
}

func createLogPath(logPath string) {
	_, err := os.Stat(logPath)
	if os.IsNotExist(err) {
		errDir := os.MkdirAll(logPath, 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}
}
